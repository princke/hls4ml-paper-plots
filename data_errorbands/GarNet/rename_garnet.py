import os

def switch_labels_garnet():
    for file in os.listdir():
        os.rename(file, file.replace("_KFold_10", ""))
    for file in os.listdir():    
        os.rename(file, file.replace(".csv", "_KFold_10.csv"))


def main():
    switch_labels_garnet()


if __name__ == "__main__":
    main()
 
