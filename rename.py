import os

def format_garnet():
    for file in os.listdir():
        try:
            os.rename(file, file.replace("garnet", "GarNet"))
        except:
            pass

        try:
            os.rename(file, file.replace("Garnet", "GarNet"))
        except:
            pass


def rename_metric():
    for file in os.listdir():
        os.rename(file, file.replace("performance","FPR@TPR"))


def main():
    format_garnet()
    rename_metric()


if __name__ == "__main__":
    main()
